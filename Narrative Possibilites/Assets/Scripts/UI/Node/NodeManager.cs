﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NodeManager : MonoBehaviour {

    public Vector3 mouseWorldPos = new Vector3();
    public Port connectingOutput = null;
    public GameObject nodePref;
    public List<Node> allNodes;

    // Use this for initialization
    void Start () {
        allNodes = new List<Node>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 mousePos = Input.mousePosition;
        mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));
        if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftControl))
        {
            if (MouseNotOverNodes() && !EventSystem.current.IsPointerOverGameObject())
            {
                if (connectingOutput != null)
                {
                    connectingOutput.activeConnection.RemoveConnection();
                    connectingOutput.activeConnection = null;
                    connectingOutput = null;
                }
                GameObject newNode = Instantiate(nodePref, mouseWorldPos, nodePref.transform.rotation) as GameObject;
            }
        }
    }

    public bool MouseNotOverNodes()
    {
        bool result = true;
        foreach (Node node in allNodes)
        {
            if (node.MouseOverNode())
            {
                result = false;
            }
        }
        return result;
    }
}
