﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    private enum NodePart
    {
        BODY,
        INPUT,
        OUTPUT,
    }

    public NodeManager nodeManager;
    public List<Port> inputs;
    public List<Port> outputs;
    [SerializeField] private GameObject relationshipPref;
    [SerializeField] private Sprite selectedImg, unselectedImg;

    private Camera camera;
    private Collider bounds;
    private Vector2 offset;
    public bool isMoving;

    // Use this for initialization
    void Start()
    {
        bounds = GetComponent<Collider>();
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        nodeManager = GameObject.FindGameObjectWithTag("NodeManager").GetComponent<NodeManager>();
        nodeManager.allNodes.Add(this);
    }

    public bool MouseOverNode()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        return Physics.Raycast(ray);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(new Vector2(bounds.bounds.center.x - (bounds.bounds.size.x / 2), bounds.bounds.center.y), new Vector2(bounds.bounds.center.x + (bounds.bounds.size.x / 2), bounds.bounds.center.y), Color.red);
        Debug.DrawLine(new Vector2(bounds.bounds.center.x, bounds.bounds.center.y - (bounds.bounds.size.y / 2)), new Vector2(bounds.bounds.center.x, bounds.bounds.center.y + (bounds.bounds.size.y / 2)), Color.red);

        if (MouseOverNode())
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    if (CheckNodePartHit(NodePart.BODY, hit))
                    {
                        isMoving = true;
                        offset = new Vector2(transform.position.x - nodeManager.mouseWorldPos.x, transform.position.y - nodeManager.mouseWorldPos.y);
                    }
                    if (CheckNodePartHit(NodePart.OUTPUT, hit))
                    {
                        foreach (Port output in outputs)
                        {
                            if (hit.collider.gameObject.GetInstanceID() == output.gameObject.GetInstanceID())
                            {
                                output.StartConnecting();
                                nodeManager.connectingOutput = output;
                            }
                        }
                    }
                    if (CheckNodePartHit(NodePart.INPUT, hit))
                    {
                        foreach (Port input in inputs)
                        {
                            if (hit.collider.gameObject.GetInstanceID() == input.gameObject.GetInstanceID())
                            {
                                if (nodeManager.connectingOutput != null)
                                {
                                    nodeManager.connectingOutput.MakeConnection(input);
                                    nodeManager.connectingOutput = null;
                                }
                            }
                        }
                    }
                    Debug.DrawLine(camera.transform.position, hit.point, Color.red);
                }
            }
        }

        if (isMoving)
        {
            if (Input.GetMouseButton(0))
            {
                //follow mouse
                transform.position = new Vector2(nodeManager.mouseWorldPos.x + offset.x, nodeManager.mouseWorldPos.y + offset.y);
            }
            else
            {
                isMoving = false;
                offset = Vector2.zero;
            }
        }
    }

    private bool CheckNodePartHit(NodePart part, RaycastHit hit)
    {
        switch (part)
        {
            case NodePart.BODY:
                if (hit.collider.tag == "Node" && hit.collider.tag != "Input" && hit.collider.tag != "Output" && hit.collider.gameObject.GetInstanceID() == this.gameObject.GetInstanceID())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            case NodePart.INPUT:
                if (hit.collider.tag != "Node" && hit.collider.tag == "Input" && hit.collider.tag != "Output" && hit.collider.gameObject.GetInstanceID() != this.gameObject.GetInstanceID())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            case NodePart.OUTPUT:
                if (hit.collider.tag != "Node" && hit.collider.tag != "Input" && hit.collider.tag == "Output" && hit.collider.gameObject.GetInstanceID() != this.gameObject.GetInstanceID())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            default:
                return false;
        }
    }
}
