﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Port : MonoBehaviour
{

    public enum PortType
    {
        INPUT,
        OUTPUT
    }

    public Node self;
    public Connection activeConnection;
    public List<Connection> connections;
    public PortType type = PortType.INPUT;
    [SerializeField] private GameObject connectionPref, arrowPref;

    // Use this for initialization
    void Start () {
        self = transform.parent.gameObject.GetComponent<Node>();
    }

    public void StartConnecting()
    {
        activeConnection = ScriptableObject.CreateInstance<Connection>();
        GameObject newLine = Instantiate(connectionPref, transform.position, connectionPref.transform.rotation) as GameObject;
        GameObject newArrow = Instantiate(arrowPref, transform.position, arrowPref.transform.rotation) as GameObject;
        activeConnection.init(this, newLine, newArrow);
    }

    public void MakeConnection(Port input)
    {
        if (type == PortType.OUTPUT && activeConnection != null)
        {
            //check for previous connection
            bool isPreviousPartner = false;
            foreach (Connection connection in connections)
            {
                if (input == connection.input)
                {
                    isPreviousPartner = true;
                }
            }
            foreach (Connection connection in input.connections)
            {
                if (this == connection.output)
                {
                    isPreviousPartner = true;
                }
            }
            if (!isPreviousPartner)
            {
                activeConnection.input = input;
                activeConnection.state = ConnectionState.CONNECTED;
                connections.Add(activeConnection);
                input.connections.Add(activeConnection);
                activeConnection = null;
            }
        }
    }

    void Update()
    {
        if (type == PortType.OUTPUT)
        {
            if (activeConnection != null)
            {
                activeConnection.update(self.nodeManager.mouseWorldPos);
            }
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i].update(self.nodeManager.mouseWorldPos);
            }
        }
    }
}