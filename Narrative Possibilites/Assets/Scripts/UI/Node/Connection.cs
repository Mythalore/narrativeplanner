﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConnectionState
{
    CONNECTING,
    CONNECTED
}

public class Connection : ScriptableObject {

    public ConnectionState state;
    public Port output, input = null;
    public GameObject line = null, arrow = null;

    // Use this for initialization
    public void init(Port output, GameObject line, GameObject arrow)
    {
        state = ConnectionState.CONNECTING;
        this.output = output;
        this.line = line;
        this.arrow = arrow;
    }

    // Update is called once per frame
    public void update(Vector3 mousePos)
    {
        switch (state)
        {
            case ConnectionState.CONNECTING:
                PositionConnection(output.transform.position, mousePos);
                break;
            case ConnectionState.CONNECTED:
                PositionConnection(output.transform.position, input.transform.position);
                break;
        }
    }

    public void PositionConnection(Vector2 start, Vector2 finish)
    {
        line.transform.position = start - ((start - finish) / 2);
        arrow.transform.position = start - ((start - finish) / 2);
        //rotation
        float AngleRad = Mathf.Atan2(start.y - line.transform.position.y, start.x - line.transform.position.x);
        float AngleDeg = ((180 / Mathf.PI) * AngleRad - 90);
        line.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        arrow.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        //size
        line.transform.localScale = new Vector3(0.1f, (start - finish).magnitude);
    }

    public void RemoveConnection()
    {
        Destroy(line);
        Destroy(arrow);
        Destroy(this);
    }
}
