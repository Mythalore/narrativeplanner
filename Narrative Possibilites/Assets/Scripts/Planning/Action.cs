﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType
{
    MOVE, //actor from location 1 to location 2
    INTERACT, //actor in location uses item in location
    CONVERSE //actor 1 in location influences actor 2 in location
}

[System.Serializable]
public class Action
{
    public ActionType actionType;

    public virtual List<ActedState> GetActedWorldStates(WorldState worldState) { return null; }

    //public virtual object Clean() { return null; }
}
