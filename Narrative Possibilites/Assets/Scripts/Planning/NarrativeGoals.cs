﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NarrativeGoals {

    private List<CharacterGoal> characterGoals = new List<CharacterGoal>();

	// Use this for initialization
	public bool CheckGoals (WorldState worldState) {
        bool isGoal = true;
        //character goals
        for (int i = 0; i < characterGoals.Count; i++)
        {
            if (!characterGoals[i].CheckGoal(worldState))
            {
                isGoal = false;
            }
        }
        return isGoal;
    }
	
	// Update is called once per frame
	public void AddCharacterGoal (Actor actor, Location location = null, States state = States.ALIVE, Emotions emotion = Emotions.NORMAL, Feelings feeling = Feelings.FINE) {
        CharacterGoal newCharacterGoal = new CharacterGoal(actor, location, state, emotion, feeling);
        characterGoals.Add(newCharacterGoal);
    }
}
