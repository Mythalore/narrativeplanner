﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ActedState
{
    public ActionType actionType;
    public WorldState initialWorldState;
    public WorldState actedWorldState;
    public string actionSummary;

    public ActedState()
    {

    }

    public ActedState(WorldState initialWorldState, ActionType actionType, string actionSummary, WorldState actedWorldState)
    {
        this.initialWorldState = initialWorldState;
        this.actionType = actionType;
        this.actionSummary = actionSummary;
        this.actedWorldState = actedWorldState;
    }
}

[System.Serializable]
public class WorldState
{
    private static int idCount = 0;
    public int id;
    public int generation = 0;
    public List<Actor> actors;
    public List<Location> locations;
	public List<Item> items;
    public List<ActedState> pathWorldStates;
    public List<ActedState> childActedWorldStates;

    public WorldState()
    {
        actors = new List<Actor>();
        locations = new List<Location>();
        items = new List<Item>();
        pathWorldStates = new List<ActedState>();
        childActedWorldStates = new List<ActedState>();
    }

    public object Clone()
    {
        WorldState cloneWorldState = new WorldState();
        for (int i = 0; i < actors.Count; i++)
        {
            Actor newActor = (Actor)actors[i].Clone();
            cloneWorldState.actors.Add(newActor);
        }
        for (int i = 0; i < locations.Count; i++)
        {
            Location newLocation = (Location)locations[i].Clone();
            cloneWorldState.locations.Add(newLocation);
        }
        for (int i = 0; i < items.Count; i++)
        {
            Item newItem = (Item)items[i].Clone();
            cloneWorldState.items.Add(newItem);
        }
        //get copy of world states so far
        List<ActedState> clonedPathWorldStates = new List<ActedState>();
        for (int i = 0; i < pathWorldStates.Count; i++)
        {
            ActedState newActedState = pathWorldStates[i];
            clonedPathWorldStates.Add(newActedState);
        }
        cloneWorldState.pathWorldStates = clonedPathWorldStates;
        cloneWorldState.childActedWorldStates = new List<ActedState>();
        cloneWorldState.generation = generation+1;
        cloneWorldState.id = ++WorldState.idCount;
        return cloneWorldState;
    }

    public void AddActor(Actor actor)
    {
        if (!actors.Contains(actor))
        {
            actors.Add(actor);
        }
    }

    public void AddLocation(Location location)
    {
        if (!locations.Contains(location))
        {
            locations.Add(location);
        }
    }

    public void AddItem(Item item)
    {
        if (!items.Contains(item))
        {
            items.Add(item);
        }
    }

    public void AddActedWorldState(ActedState acted)
    {
        if (!childActedWorldStates.Contains(acted))
        {
            childActedWorldStates.Add(acted);
        }
    }

    public Location LocationByID(float id){
		Location location = null;
		for(int i = 0; i < locations.Count; i++){
			if(locations[i].id == id){
				location = locations[i];
                break;
			}
		}
		return location;
	}

	public Actor ActorByID(float id){
		Actor actor = null;
		for(int i = 0; i < actors.Count; i++){
			if(actors[i].id == id){
				actor = actors[i];
                break;
            }
		}
		return actor;
	}

	public Item ItemByID(float id){
		Item item = null;
		for(int i = 0; i < items.Count; i++){
			if(items[i].id == id){
				item = items[i];
                break;
            }
		}
		return item;
	}
}
