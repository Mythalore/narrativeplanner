﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterGoal {

    public Actor actor;
    public Location location = null;
    public States state = States.ALIVE;
    public Emotions emotion = Emotions.NORMAL;
    public Feelings feeling = Feelings.FINE;

    public CharacterGoal(Actor actor, Location location = null, States state = States.ALIVE, Emotions emotion = Emotions.NORMAL, Feelings feeling = Feelings.FINE)
    {
        this.actor = actor;
        this.location = location;
        this.state = state;
        this.emotion = emotion;
        this.feeling = feeling;
    }

    public bool CheckGoal(WorldState worldState)
    {
        bool goalReached = false;
        if (worldState.ActorByID(actor.id) != null)
        {
            Actor actorToCheck = worldState.ActorByID(actor.id);
            if (actorToCheck.locationID == location.id && actorToCheck.state == state && actorToCheck.emotion == emotion && actorToCheck.feeling == feeling)
            {
                goalReached = true;
            }
        }
        return goalReached;
    }
}
