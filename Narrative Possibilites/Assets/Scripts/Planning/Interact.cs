﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : Action
{
    public Interact()
    {
        actionType = ActionType.INTERACT;
    }

    public void PerformActionBasedOnItemType(List<ActedState> actedWorldStates, WorldState worldState, Actor actor, Location location, Item item)
    {
        switch (item.type)
        {
            case Type.KEY:
                //if key find adjacent room to unlock
                for (int c = 0; c < location.connectionIDs.Count; c++)
                {
                    int connectionID = location.connectionIDs[c];
                    Location connection = worldState.LocationByID(connectionID);
                    if (connection.isLocked)
                    {
                        //unlock door
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = actor.displayName + " unlocked " + connection.displayName + " with " + item.displayName + " from " + location.displayName;
                        clonedWorldState.LocationByID(connection.id).isLocked = false;
                        //Debug.Log(actor.displayName + " unlocked " + connection.displayName + " with " + item.displayName + " from " + location.displayName);

                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                }
                break;
            case Type.DRINK:
                //check if actor is thirsty
                if (actor.feeling == Feelings.THIRSTY)
                {
                    //quench actor's thirst
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " drank " + item.displayName + " in " + location.displayName;
                    clonedWorldState.ActorByID(actor.id).feeling = Feelings.FINE;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
            case Type.FOOD:
                //check if actor is hungry
                if (actor.feeling == Feelings.HUNGRY)
                {
                    //satisfy actor's hunger
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " ate " + item.displayName + " in " + location.displayName;
                    clonedWorldState.ActorByID(actor.id).feeling = Feelings.FINE;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
            case Type.TOOL:
                //if key find broken item to fix
                for (int i = 0; i < worldState.items.Count; i++)
                {
                    Item brokenItem = worldState.ItemByID(i);
                    if (!brokenItem.isUsable && brokenItem.id != item.id && brokenItem.locationID == location.id)
                    {
                        //fix item
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = actor.displayName + " fixed " + brokenItem.displayName + " with " + item.displayName + " in " + location.displayName;
                        clonedWorldState.ItemByID(brokenItem.id).isUsable = true;
                        
                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                }
                break;
            case Type.WEAPON:
                //if key find actor to hurt or murder
                for (int i = 0; i < worldState.actors.Count; i++)
                {
                    Actor victim = worldState.ActorByID(i);
                    if (victim.id != actor.id && victim.locationID == location.id && victim.hasArmour)
                    {
                        //fight actor
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = victim.displayName + " defended " + actor.displayName + "'s attack with " + item.displayName + " in " + location.displayName;
                        clonedWorldState.ActorByID(victim.id).hasArmour = false;

                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                    if (victim.id != actor.id && victim.locationID == location.id && victim.state == States.ALIVE && !victim.hasArmour)
                    {
                        //hurt actor
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = actor.displayName + " attacked " + victim.displayName + " with " + item.displayName + " in " + location.displayName;
                        clonedWorldState.ActorByID(victim.id).state = States.HURT;

                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                    if (victim.id != actor.id && victim.locationID == location.id && victim.state == States.HURT && !victim.hasArmour)
                    {
                        //hurt actor
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = actor.displayName + " killed " + victim.displayName + " with " + item.displayName + " in " + location.displayName;
                        clonedWorldState.ActorByID(victim.id).state = States.DEAD;

                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                }
                break;
            case Type.ARMOUR:
                //actor to protect
                if (!actor.hasArmour)
                {
                    //fight actor
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " protected themselves with " + item.displayName + " in " + location.displayName;
                    clonedWorldState.ActorByID(actor.id).hasArmour = true;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
            case Type.BED:
                //put actor to sleep
                if (actor.state != States.DEAD && actor.state != States.ASLEEP)
                {
                    //tuck actor in
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " slept on " + item.displayName + " in " + location.displayName;
                    Actor clonedActor = clonedWorldState.ActorByID(actor.id);
                    clonedActor.state = States.ASLEEP;
                    clonedActor.emotion = Emotions.NORMAL;
                    clonedActor.feeling = Feelings.THIRSTY;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
            case Type.ALARM:
                //wake actor up
                if (actor.state == States.ASLEEP)
                {
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " woke by " + item.displayName + " in " + location.displayName;
                    clonedWorldState.ActorByID(actor.id).state = States.ALIVE;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
            case Type.MEDICAL:
                //heal actor
                if (actor.state == States.HURT)
                {
                    ActedState newActedState;
                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                    string actionSummary = actor.displayName + " healed using " + item.displayName + " in " + location.displayName;
                    clonedWorldState.ActorByID(actor.id).state = States.ALIVE;

                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                    clonedWorldState.pathWorldStates.Add(newActedState);
                    actedWorldStates.Add(newActedState);
                    newActedState = null;
                }
                break;
        }
    }

    public override List<ActedState> GetActedWorldStates(WorldState worldState)
    {
        List<ActedState> actedWorldStates = new List<ActedState>();
        //validate and get actor, location and item
        for (int a = 0; a < worldState.actors.Count; a++)
        {
            Actor actor = worldState.actors[a];
            for (int l = 0; l < worldState.locations.Count; l++)
            {
                Location location = worldState.locations[l];
                for (int i = 0; i < worldState.items.Count; i++)
                {
                    Item item = worldState.items[i];
                    //check if item not broken
                    if (item.isUsable && actor.locationID == location.id && actor.state != States.DEAD && actor.state != States.ASLEEP)
                    {
                        //check for valid pick up item action
                        if (item.canMove && !item.isHeld && item.locationID == location.id)
                        {
                            //pick up item
                            ActedState newActedState;
                            WorldState clonedWorldState = (WorldState)worldState.Clone();

                            string actionSummary = actor.displayName + " picked up item " + item.displayName + " in " + location.displayName;
                            Item clonedItem = clonedWorldState.ItemByID(item.id);
                            clonedItem.ownerID = actor.id;
                            clonedItem.locationID = -1;
                            clonedItem.isHeld = true;
                            //Debug.Log(clonedItem.ownerID + " picked up item " + clonedItem.displayName + " in " + location.displayName);

                            newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                            clonedWorldState.pathWorldStates.Add(newActedState);
                            actedWorldStates.Add(newActedState);
                            newActedState = null;
                        }else
                        //check if being held
                        if (item.canMove && item.isHeld)
                        {
                            //check if actor is owner of item
                            if (item.ownerID == actor.id)
                            {
                                //perform action based on item type
                                PerformActionBasedOnItemType(actedWorldStates, worldState, actor, location, item);
                            }
                        }
                        else
                        //check if stationary
                        if (!item.canMove)
                        {
                            //check if actor and item are in the same location
                            if (item.locationID == location.id)
                            {
                                //perform action based on item type
                                PerformActionBasedOnItemType(actedWorldStates, worldState, actor, location, item);
                            }
                        }
                    }
                }
            }
        }
        return actedWorldStates;
    }
}
