﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Converse : Action
{
    public Converse()
    {
        actionType = ActionType.CONVERSE;
    }
    
    public override List<ActedState> GetActedWorldStates(WorldState worldState)
    {
        List<ActedState> actedWorldStates = new List<ActedState>();
        //validate and get actor, location and item
        for (int a1 = 0; a1 < worldState.actors.Count; a1++)
        {
            Actor speaker = worldState.actors[a1];
            for (int l = 0; l < worldState.locations.Count; l++)
            {
                Location location = worldState.locations[l];
                for (int a2 = 0; a2 < worldState.actors.Count; a2++)
                {
                    Actor listener = worldState.actors[a2];
                    if (speaker.state != States.DEAD && listener.state != States.DEAD && speaker.state != States.ASLEEP &&
                        speaker.locationID == location.id && listener.locationID == location.id && 
                        speaker.id != listener.id)
                    {
                        //interact between a1 and a2 based on their emotions
                        switch (speaker.emotion)
                        {
                            case Emotions.NORMAL:
                                //if speaker is normal wake listener up
                                if (listener.state == States.ASLEEP)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " woke " + listener.displayName + " up in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).state = States.ALIVE;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                //insite all emotions
                                for (int e = 0; e < 4; e++)
                                {
                                    if (listener.emotion == Emotions.NORMAL)
                                    {
                                        ActedState newActedState;
                                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                                        string actionSummary = speaker.displayName + " made " + listener.displayName + " " + ((Emotions)e).ToString().ToLower() + " in " + location.displayName;
                                        clonedWorldState.ActorByID(listener.id).emotion = (Emotions)e;

                                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                        clonedWorldState.pathWorldStates.Add(newActedState);
                                        actedWorldStates.Add(newActedState);
                                        newActedState = null;
                                    }
                                }
                                break;
                            case Emotions.HAPPY:
                                if (listener.emotion == Emotions.NORMAL)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " made " + listener.displayName + " happy in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.HAPPY;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                if (listener.emotion == Emotions.SAD)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " made " + listener.displayName + " angry in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.ANGRY;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                if (listener.emotion == Emotions.ANGRY)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " calmed " + listener.displayName + " down in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.NORMAL;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                break;
                            case Emotions.SAD:
                                if (listener.emotion == Emotions.NORMAL ||
                                    listener.emotion == Emotions.HAPPY)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " made " + listener.displayName + " sad in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.SAD;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                break;
                            case Emotions.ANGRY:
                                if (listener.emotion == Emotions.NORMAL)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " scared " + listener.displayName + " in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.AFRAID;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                if (listener.emotion == Emotions.SAD)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " annoyed " + listener.displayName + " in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.ANGRY;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                break;
                            case Emotions.AFRAID:
                                if (listener.emotion == Emotions.NORMAL)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " scared " + listener.displayName + " in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.AFRAID;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                if (listener.emotion == Emotions.HAPPY)
                                {
                                    ActedState newActedState;
                                    WorldState clonedWorldState = (WorldState)worldState.Clone();

                                    string actionSummary = speaker.displayName + " scared " + listener.displayName + " in " + location.displayName;
                                    clonedWorldState.ActorByID(listener.id).emotion = Emotions.AFRAID;

                                    newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                                    clonedWorldState.pathWorldStates.Add(newActedState);
                                    actedWorldStates.Add(newActedState);
                                    newActedState = null;
                                }
                                break;
                        }
                    }
                }
            }
        }
        return actedWorldStates;
    }
}
