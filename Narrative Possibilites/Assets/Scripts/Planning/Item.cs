﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Type
{
    KEY, //unlocks rooms
    DRINK, //removes thirst
    FOOD, //removes hunger
    TOOL, //fixes usable
    WEAPON, //hurts actor
    ARMOUR, //protects actor
    BED, //allows actor to sleep
    ALARM, //wakes actor up
    MEDICAL, //heals actor
}

[System.Serializable]
public class Item : System.ICloneable
{
    public static int itemIDCount = 0;
	public string displayName;
	public int id;
    public int locationID;
    public int ownerID;
    public bool canMove;
    public bool isHeld;
    public bool isUsable;
    public Type type;

    public Item(string displayName, Location initialLocation, bool canMove, bool isUsable, Type type)
    {
        this.displayName = displayName;
        this.id = itemIDCount++;
        Debug.Log("Item ID: " + id + " ID Count: " + itemIDCount);
        this.locationID = initialLocation.id;
        this.ownerID = -1;
        this.isHeld = false;
        this.canMove = canMove;
        this.isUsable = isUsable;
        this.type = type;
    }

    public Item(string displayName, Actor initialOwner, bool canMove, bool isUsable, Type type)
    {
        this.displayName = displayName;
        this.id = itemIDCount++;
        Debug.Log("Item ID: " + id + " ID Count: " + itemIDCount);
        this.locationID = -1;
        this.ownerID = initialOwner.id;
        this.isHeld = true;
        this.canMove = canMove;
        this.isUsable = isUsable;
        this.type = type;
    }

    public Item() { }

    public object Clone()
    {
        return new Item
        {
            displayName = System.String.Copy(this.displayName),
            id = this.id,
            locationID = this.locationID,
            ownerID = this.ownerID,
            isHeld = this.isHeld,
            canMove = this.canMove,
            isUsable = this.isUsable,
            type = this.type
        };
    }
}
