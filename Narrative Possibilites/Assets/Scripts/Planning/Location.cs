﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Size
{
    TINY,
    SMALL,
    MEDIUM,
    LARGE,
    HUGE,
}

public enum Environment
{
	INSIDE,
	OUTSIDE,
}

[System.Serializable]
public class Location : System.ICloneable
{
    public static int locationIDCount = 0;
    public string displayName;
	public int id;
    public bool isLocked;
    public Size size;
	public Environment environment;
    public List<int> connectionIDs;

    public Location(string displayName, bool isLocked, Size size, Environment environment)
    {
        this.displayName = displayName;
        this.id = locationIDCount++;
        Debug.Log("Location ID: " + id + " ID Count: " + locationIDCount);
        this.isLocked = isLocked;
        this.size = size;
        this.environment = environment;
        connectionIDs = new List<int>();
    }

    public Location(string displayName, Size size, Environment environment)
    {
        this.displayName = displayName;
        this.id = locationIDCount++;
        Debug.Log("Location ID: " + id + " ID Count: " + locationIDCount);
        isLocked = false;
        this.size = size;
        this.environment = environment;
        connectionIDs = new List<int>();
    }

    public Location()
    {
        connectionIDs = new List<int>();
    }

    public object Clone()
    {
        List<int> clonedConnectionIDs = new List<int>();
        for (int i = 0; i < connectionIDs.Count; i++)
        {
            clonedConnectionIDs.Add(connectionIDs[i]);
        }
        return new Location
        {
            displayName = System.String.Copy(this.displayName),
            id = this.id,
            isLocked = this.isLocked,
            size = this.size,
            environment = this.environment,
            connectionIDs = clonedConnectionIDs,
        };
    }

    // Add connection between two locations
    public void AddConnection (int connectionID)
    {
        if (!connectionIDs.Contains(connectionID))
            connectionIDs.Add(connectionID);
    }
}
