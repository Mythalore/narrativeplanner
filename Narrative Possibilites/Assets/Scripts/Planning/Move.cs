﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Traverse : Action
{
    public Traverse()
    {
        actionType = ActionType.MOVE;
    }

    public override List<ActedState> GetActedWorldStates(WorldState worldState) {
        List<ActedState> actedWorldStates = new List<ActedState>();
        //validate and get locations
        for (int i = 0; i < worldState.actors.Count; i++)
        {
            Actor actor = worldState.actors[i];
            for (int l1 = 0; l1 < worldState.locations.Count; l1++)
            {
                Location location1 = worldState.locations[l1];
                for (int l2 = 0; l2 < worldState.locations.Count; l2++)
                {
                    Location location2 = worldState.locations[l2];
                    //check if all actors are in the first location and can move
                    if (location1 != location2 && location1.connectionIDs.Contains(location2.id) &&
                        actor.locationID == location1.id && actor.canMove && !location2.isLocked
                        && actor.state != States.DEAD && actor.state != States.ASLEEP)// && actor.lastLocationID != location2.id)
                    {
                        //conduct move action
                        ActedState newActedState;
                        WorldState clonedWorldState = (WorldState)worldState.Clone();

                        string actionSummary = actor.displayName + " moved from " + location1.displayName + " to " + location2.displayName;
                        Actor clonedActor = clonedWorldState.ActorByID(actor.id);
                        clonedActor.locationID = location2.id;
                        //clonedActor.lastLocationID = location1.id;

                        newActedState = new ActedState(worldState, actionType, actionSummary, clonedWorldState);
                        clonedWorldState.pathWorldStates.Add(newActedState);
                        actedWorldStates.Add(newActedState);
                        newActedState = null;
                    }
                }
            }
        }
        return actedWorldStates;
    }
}
