﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Emotions
{
    NORMAL,
    HAPPY,
    SAD,
    ANGRY,
    AFRAID,
}

public enum States
{
    ALIVE,
    ASLEEP,
    HURT,
    DEAD,
}

public enum Feelings
{
    FINE,
    HUNGRY,
    THIRSTY,
    TIRED,
    COLD,
    HOT,
}

[System.Serializable]
public class Actor : System.ICloneable
{
    public static int actorIDCount = 0;
    public string displayName;
	public int id;
    public int locationID;
    //public int lastLocationID;
    public bool canMove;
    public bool hasArmour;
    public States state;
    public Emotions emotion;
    public Feelings feeling;

    public Actor(string displayName, Location initialLocation, bool canMove, States state, Emotions emotion, Feelings feeling)
    {
        this.displayName = displayName;
        this.id = actorIDCount++;
        Debug.Log("Actor ID: " + id + " ID Count: " + actorIDCount);
        this.locationID = initialLocation.id;
        //this.lastLocationID = -1;
        this.canMove = canMove;
        this.state = state;
        this.emotion = emotion;
        this.feeling = feeling;
    }

    public Actor(){ }

    public object Clone()
    {
        return new Actor
        {
            displayName = System.String.Copy(this.displayName),
            id = this.id,
            locationID = this.locationID,
            //lastLocationID = this.lastLocationID,
            canMove = this.canMove,
            state = this.state,
            emotion = this.emotion,
            feeling = this.feeling
        };
    }
}
