﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryPlanner : MonoBehaviour {

    public int maxAttempts;
    public NarrativeGoals narrativeGoals;
    public List<Action> actions = new List<Action>();
    private List<WorldState> worldStatesNew = new List<WorldState>();
    private List<WorldState> worldStatesOld = new List<WorldState>();
    public WorldState goalState;

    // Use this for initialization
    void Start () {

        //create actions
        Traverse moveAction = new Traverse();
        actions.Add(moveAction);
        Interact interactAction = new Interact();
        actions.Add(interactAction);
        Converse converseAction = new Converse();
        actions.Add(converseAction);
        //initial world state
        WorldState initialWorldState = new WorldState();

        //locations with connections between
        Location lift = new Location("Lift", Size.TINY, Environment.INSIDE);

        Location sleepingPods = new Location("Sleeping Pods", Size.LARGE, Environment.INSIDE);
        sleepingPods.AddConnection(lift.id);
        lift.AddConnection(sleepingPods.id);

        Location concourse = new Location("Main Concourse", Size.HUGE, Environment.INSIDE);
        concourse.AddConnection(lift.id);
        lift.AddConnection(concourse.id);

        Location bar = new Location("Bar", Size.SMALL, Environment.INSIDE);
        bar.AddConnection(concourse.id);
        concourse.AddConnection(bar.id);

        Location cabin = new Location("Cabin", Size.SMALL, Environment.INSIDE);
        cabin.AddConnection(lift.id);
        lift.AddConnection(cabin.id);

        Location bridge = new Location("Bridge", Size.MEDIUM, Environment.INSIDE);
        bridge.AddConnection(lift.id);
        lift.AddConnection(bridge.id);

        //actors
        Actor jim = new Actor("Jim", concourse, true, States.ALIVE, Emotions.NORMAL, Feelings.FINE);
        Actor aurora = new Actor("Aurora", concourse, true, States.ALIVE, Emotions.ANGRY, Feelings.FINE);
        Actor arthur = new Actor("Arthur", bar, false, States.ALIVE, Emotions.HAPPY, Feelings.FINE);

        //items
        Item glass = new Item("Glass", bar, true, true, Type.WEAPON);

        //goals
        narrativeGoals = new NarrativeGoals();
        narrativeGoals.AddCharacterGoal(aurora, bar, States.ALIVE);
        narrativeGoals.AddCharacterGoal(jim, bar, States.HURT, Emotions.SAD);

        //add locations to initial world state
        initialWorldState.AddLocation(sleepingPods);
        initialWorldState.AddLocation(concourse);
        initialWorldState.AddLocation(bar);
        initialWorldState.AddLocation(cabin);
        initialWorldState.AddLocation(lift);
        initialWorldState.AddLocation(bridge);

        //add actors to initial world state
        initialWorldState.AddActor(jim);
        initialWorldState.AddActor(aurora);
        initialWorldState.AddActor(arthur);

        //add item to initial world state
        initialWorldState.AddItem(glass);

        //add initial world state to planner list
        worldStatesNew.Add(initialWorldState);

    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            int attempts = 0;
            while (attempts < maxAttempts)
            {
                PopulateGraph();
                goalState = CheckGoal();
                if (goalState != null)
                {
                    string goalPath = "";
                    for (int i = 0; i < goalState.pathWorldStates.Count; i++)
                    {
                        if (i == goalState.pathWorldStates.Count-1)
                        {
                            goalPath += "" + goalState.pathWorldStates[i].actionSummary;
                        }
                        else
                        {
                            goalPath += "" + goalState.pathWorldStates[i].actionSummary + " -> ";
                        }
                    }
                    Debug.LogError("Goal Found: " + goalPath);
                    break;
                }
                attempts++;
            }
            if (attempts >= maxAttempts)
            {
                Debug.LogError("Goal Not Found");
            }
        }
    }

    void PopulateGraph()
    {
        //run through all possible actions get acted state if possible
        //get world state
        WorldState currentWorldState = worldStatesNew[0];

        for(int a = 0; a < actions.Count; a++)
        {
            //check actions
            List<ActedState> currentActedStates = new List<ActedState>();
            currentActedStates = actions[a].GetActedWorldStates(currentWorldState);
            //add to acted states of world state
            //add children to new world states list
            for (int i = 0; i < currentActedStates.Count; i++)
            {
                currentWorldState.AddActedWorldState(currentActedStates[i]);
                worldStatesNew.Add(currentActedStates[i].actedWorldState);
            }
        }

        //remove old world state and add it to old list
        worldStatesNew.Remove(currentWorldState);
        worldStatesOld.Add(currentWorldState);
        if (worldStatesNew.Count <= 0)
        {
            Debug.LogError("path ended prematurely");
        }
    }

    WorldState CheckGoal()
    {
        WorldState goalState = null;
        for (int i = 0; i < worldStatesOld.Count; i++)
        {
            for (int j = 0; j < worldStatesOld[i].childActedWorldStates.Count; j++)
            {
                WorldState stateToCheck = worldStatesOld[i].childActedWorldStates[j].actedWorldState;

                if (narrativeGoals.CheckGoals(stateToCheck))
                {
                    goalState = stateToCheck; //problem is with wrong world state being passed through as the goal
                    break;
                }
            }
            if (goalState != null) break;
        }
        return goalState;
    }
}
